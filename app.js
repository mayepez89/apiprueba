var express = require('express');
var app = express();
var routes = require('./routes');
var cors = require('cors');
const bodyParser = require('body-parser');
const md5 = require('md5');
const async = require('async');
//incluimos redis a nuestro script
var redis = require('redis');
var Request = require("request");

// para parcear el body
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cors());

//creamos un cliente
var redisClient = redis.createClient();
redisClient.on('connect', function () {
  console.log('Conectado a redisClient');
});
//-----------------------
// registrar a un usuario
app.post('/user/new',(req, res) => {
  const user = req.body;
  user.id = md5(user.username);
  user.password = md5(user.password);
  redisClient.exists(user.id, (error, reply) => {
    console.log("reply: " + reply)
    if (reply === 1) {
      return res.json({ status: 400, message: 'El usuario ya existe' });
    }
    redisClient.hset(
      user.id, [
        'id', user.id,
      'username', user.username,
      'password', user.password,
      'email', user.email,
      'telefono', user.telefono
    ]
      , (error, result) => {
        if (error) {
          return res.json({ result, status: 400, message: 'Algo salio mal', error });
        }
        return res.json({ status: 200, message: 'Usuario creado', user });
      }
    );
    console.log('Got body:', user);
  })
});

// obtener todos los usuarios registrado
app.get('/users',function (req, res) {
  redisClient.keys('*', (err, keys) => {
    if (err) {
      return res.json({ status: 400, message: 'no se pudieron obtener usuarios', err });
    }
    if (keys) {
      async.map(keys, (key, cb) => {
        redisClient.hgetall(key, (error, value) => {
          if (error) return res.json({ status: 400, message: 'Algo salió mal ups', error });
          const user = {};
          user.id = key;
          user.data = value;
          cb(null, user);
        });
      }, (error, users) => {
        if (error) return res.json({ status: 400, message: 'Algo salió mal', error });
        res.json(users);
      });
    }
  });
});

//inicio de sesion
app.get('/user', function (req, res) {
  const userId = req.query;
  const userU = md5(userId.username);
  const userP = md5(userId.password);
  redisClient.hgetall(userU, (err, user) => {
    if (err) {
      return res.json({ status: 400, message: 'Algo salio mal', err });
    }
    if (user) {
      if (userP === user.password) {
        //---------- expire
        redisClient.hset(
          'sessions', [
          'sessionId', userU
        ]);
        redisClient.expire('sessions', 120);
        //-----------
        return res.json({ user, status: 200, message: 'Bienvenido' })
      } else {
        return res.json({ status: 400, message: 'la contraseña no es valida' });
      }

    } else {
      return res.json({ status: 400, message: 'no existe el usuario' });
    }

  });
})

//mostrar Rick y Morty
app.get('/mostrar/:id/:page', function (req, res) {
  const param = req.params;
  var url = "";
  redisClient.hgetall('sessions', (error, value) => {
    if (error) {
      return res.json({ status: 400, message: 'Algo salió mal ups', error });
    } else {
      if (value) {
        if (value.sessionId == param.id) {
          if(param.page === '1'){
            url = "https://rickandmortyapi.com/api/character/";
          }else{
            url = "https://rickandmortyapi.com/api/character/" + "?page=" + parseInt(param.page);
          }
          Request.get(url, (error, response, body) => {
            if (error) {
              return console.dir(error);
            }
            const data = JSON.parse(body);
            return res.json({status: 200, message: 'sesion valida', data})
          });
        }else{
          return res.json({ status: 400, message: 'sesion expirada', error });
        }
      } else {
        return res.json({ status: 400, message: 'sesion expirada', error });
      }
    }

    const user = {};
    user.data = value;
  });
});

app.listen(8080, () => console.log('Started server at http://localhost:8080!'));