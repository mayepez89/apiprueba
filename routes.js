var express = require('express')
var router = express.Router()

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now())
    next()
})
// define the home page route
router.post('/new', function (req, res, next) {
    res.send('Nuevo usuarios')
  })
// define the about route
router.get('/all', function (req, res) {
    res.send('Todos los usuarios')
})

module.exports = router